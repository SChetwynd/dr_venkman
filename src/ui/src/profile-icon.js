import React from 'react';
import { buildUrl } from './services.js';
import('./profile-icon.css');

export default class ProfileIcon extends React.Component {
    render() {
        return (
            <img
                className="profile__img"
                src={buildUrl(this.props.url)}
                alt={this.props.alt}
            />
        );
    }
}
