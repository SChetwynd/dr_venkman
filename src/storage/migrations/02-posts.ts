import { Knex } from 'knex';
import debug from 'debug';

const logger = debug('MIGRATIONS');

export async function up(knex: Knex) {
    logger('Posts Up');
    await knex.raw(`
        CREATE TABLE posts (
            id int NOT NULL AUTO_INCREMENT,
            title varchar(255) NOT NULL,
            body varchar(255) NOT NULL,
            user int NOT NULL,
            PRIMARY KEY (id),
            FOREIGN KEY (user) REFERENCES users(id)
        )
    `);
}

export async function down(knex: Knex) {
    logger('Posts Down');
    await knex.raw(`DROP TABLE posts`);
}
