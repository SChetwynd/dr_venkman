FROM node:slim
WORKDIR /app
COPY ./package.json ./package.json
RUN npm install

RUN mkdir ./tmp_ui
COPY ./src/ui/package.json ./tmp_ui
WORKDIR /app/tmp_ui
RUN npm install

WORKDIR /app
COPY ./ ./
RUN mv ./tmp_ui/node_modules ./src/ui/node_modules
RUN npm run build

WORKDIR /app/src/ui
RUN npm run build

WORKDIR /app
RUN mkdir ./migrations
RUN mv ./dist/storage/migrations/*.js ./migrations/
RUN mv ./src/ui/build/* ./dist/server/routes/static/

RUN rm -r ./src

EXPOSE 3000
ENTRYPOINT ["./start.sh"]
