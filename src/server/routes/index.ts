import { UseCases } from '../../types';
import buildComments from './comments.js';
import buildUsers from './users.js';

export default function buildRoutes(useCases: UseCases) {
    return [buildComments(useCases), buildUsers(useCases)];
}
