import express from 'express';

import { UseCases } from '../../types';

const router = express.Router();

export default function buildComments(useCases: UseCases) {
    router.get('/comments', async (request, response) => {
        return response.json(
            await useCases.comments.getComments(Number(request.query.post))
        );
    });

    router.post('/comments', async (request, response) => {
        return response.json(
            await useCases.comments.createComment(
                request.body.post,
                request.body.user,
                request.body.body,
                request.body.replyTo
            )
        );
    });

    router.post('/comments/:commentID/upvote', async (request, response) => {
        return response.json(
            await useCases.comments.upvote(Number(request.params.commentID))
        );
    });

    return router;
}
