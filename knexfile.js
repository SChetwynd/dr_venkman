import config_ from './dist/config.js';

export default {
    client: config_.dbType,
    connection: {
        host: config_.dbHost,
        user: config_.dbUser,
        password: config_.dbPassword,
        database: config_.dbDatabase,
    },
};
