import { Knex } from 'knex';
import debug from 'debug';

const logger = debug('MIGRATIONS');

export async function up(knex: Knex) {
    logger('Replies Up');
    await knex.raw(`
        ALTER TABLE comments ADD COLUMN reply_to int;
    `);
    await knex.raw(`
        ALTER TABLE comments ADD CONSTRAINT FK_ReplyTo FOREIGN KEY (reply_to) REFERENCES comments(id);
    `);
}

export async function down(knex: Knex) {
    logger('Replies Down');
    await knex.raw(`
        ALTER TABLE comments DROP FOREIGN KEY FK_ReplyTo;
    `);
    await knex.raw(`
        ALTER TABLE comments DROP column reply_to;
    `);
}
