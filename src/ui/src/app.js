import React from 'react';
import Comment from './comment-element.js';
import ProfileIcon from './profile-icon.js';
import { postComment, getComments, getUser } from './services.js';
import('./app.css');

export default class App extends React.Component {
    constructor(properties) {
        super(properties);
        this.state = {
            comments: [],
            user: {},
            newComment: '',
        };
    }

    componentDidMount() {
        this.getData();
        this.getUser();
    }

    async getData() {
        this.setState({
            comments: await getComments(),
        });
    }
    async getUser() {
        this.setState({
            user: await getUser(),
        });
    }

    render() {
        return (
            <div className="app__main">
                <div className="app__content">
                    <h1 className="app__h1">Discussion</h1>
                    <span className="app__add-comment">
                        <ProfileIcon
                            url={this.state.user.image_url}
                            alt={this.state.user.username}
                        />
                        <input
                            className="app__input"
                            type="text"
                            id="new_comment"
                            name="comment"
                            placeholder="What are your thoughts?"
                            value={this.state.newComment}
                            onChange={this.updateComment.bind(this)}
                        ></input>
                        <button
                            className="app__button"
                            id="comment_button"
                            onClick={this.postComment.bind(this)}
                        >
                            Comment
                        </button>
                    </span>
                    <hr className="app__hr" />
                    <div id="comment_holder">
                        {this.state.comments.map((comment, index) => (
                            <Comment
                                getData={this.getData.bind(this)}
                                key={index}
                                firstname={comment.firstname}
                                surname={comment.surname}
                                username={comment.username}
                                imageURL={comment.image_url}
                                timestamp={comment.timestamp}
                                body={comment.body}
                                votes={comment.votes}
                                commentId={comment.comment_id}
                                replies={comment.replies}
                                userId={this.state.user.id}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }

    updateComment(event) {
        this.setState({ newComment: event.target.value });
    }

    async postComment() {
        await postComment(this.state.user.id, this.state.newComment);
        this.setState({ newComment: '' });
        this.getData();
    }
}
