import debug from 'debug';
import { Knex } from 'knex';

const logger = debug('Storage:Posts');

type PostSummary = {
    id: number;
    title: string;
    user: number;
};

type Post = {
    id: number;
    title: string;
    body: string;
    user: number;
};

export class Posts {
    private knex: Knex;

    constructor(knexInst: Knex) {
        this.knex = knexInst;
    }

    public getAPost(id: number): Promise<Post | undefined> {
        logger(`Querying for ${id} post`);
        return this.knex<Post>('posts')
            .select('id')
            .select('title')
            .select('body')
            .select('user')
            .where({
                id,
            })
            .first();
    }

    public getPosts(): Promise<PostSummary> {
        logger('Getting all posts');
        return this.knex<PostSummary>('posts')
            .select('id')
            .select('title')
            .select('user');
    }
}

let singleton: Posts;

export function postSchema(knexInst: Knex): Posts {
    if (singleton === undefined) {
        logger('Creating new Posts singleton');
        singleton = new Posts(knexInst);
    }

    return singleton;
}
