import debug from 'debug';
import { Knex } from 'knex';

const logger = debug('Storage:Comments');

export interface Comment {
    comment_id: number;
    user: number;
    post: number;
    timestamp: Date;
    body: string;
    votes: number;
    user_id: number;
    username: string;
    firstname: string;
    surname: string;
    image_url: string;
    reply_to: string | null;
}

type RawComment = {
    user: number;
    post: number;
    body: string;
    reply_to?: number;
};

export class Comments {
    private knex: Knex;

    constructor(knexInst: Knex) {
        this.knex = knexInst;
    }

    public getComments(post: number): Promise<Comment[]> {
        logger(`Querying for comments for ${post} post`);
        return this.knex<Comment>('comments')
            .where({ post })
            .select([
                'comments.id AS comment_id',
                'user',
                'post',
                'timestamp',
                'body',
                'votes',
                'users.id AS user_id',
                'username',
                'firstname',
                'surname',
                'image_url',
                'reply_to',
            ])
            .join('users', 'comments.user', 'users.id');
    }

    public getComment(commentId: number): Promise<Comment> {
        logger(`Querying for comment ${commentId}`);
        return this.knex<Comment>('comments')
            .where('comments.id', commentId)
            .select([
                'comments.id AS comment_id',
                'user',
                'post',
                'timestamp',
                'body',
                'votes',
                'users.id AS user_id',
                'username',
                'firstname',
                'surname',
                'image_url',
                'reply_to',
            ])
            .join('users', 'comments.user', 'users.id')
            .first();
    }

    public createComments(comments: RawComment[]): Promise<void> {
        logger('Creating new comment');
        return this.knex('comments').insert(comments);
    }

    public upvote(commentId: number): Promise<unknown> {
        logger(`Upvoting comment: ${commentId}`);
        return this.knex('comments')
            .where({
                id: commentId,
            })
            .increment('votes');
    }
}

let singleton: Comments;

export function commentSchema(knexInst: Knex): Comments {
    if (singleton === undefined) {
        logger('Creating new Comments singleton');
        singleton = new Comments(knexInst);
    }

    return singleton;
}
