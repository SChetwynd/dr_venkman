import debug from 'debug';
import express from 'express';
import { createServer } from 'node:http';
import cors from 'cors';

import config from '../config.js';
import { UseCases, EventBus } from '../types';

import buildRoutes from './routes/index.js';
import initWebSocketServer from './websockets/index.js';

const logger = debug('SERVER');

export default function buildServer(useCases: UseCases, eventBus: EventBus) {
    logger('Starting server');
    const app = express();

    const server = createServer(app);

    initWebSocketServer(server, eventBus);

    app.use(cors());
    app.use(express.json());

    app.use('/api', buildRoutes(useCases));
    app.use('/', express.static('./dist/server/routes/static'));

    app.use(
        (
            error: Error,
            _request: express.Request,
            response: express.Response,
            _next: express.NextFunction
        ) => {
            response.sendStatus(500);
        }
    );

    server.listen(config.port, () => {
        logger(`Server listening on port: ${config.port}`);
    });
}
