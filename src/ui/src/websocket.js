import { buildUrl } from './services.js';

export const webSocket = new WebSocket(
    `ws://${buildUrl('').replace(/^https?:\/\//, '')}`
);
