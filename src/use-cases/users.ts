import { DatabaseConnection } from '../types';

export class UsersUseCases {
    private databaseConnection: DatabaseConnection;

    constructor(databaseConnection: DatabaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    getUsers() {
        return this.databaseConnection.users.getAllUsers();
    }

    getAUser(username: string) {
        return this.databaseConnection.users.getAUser(username);
    }

    createAUser(
        username: string,
        firstname: string,
        surname: string,
        imageUrl: string
    ) {
        return this.databaseConnection.users.createUsers([
            {
                username,
                firstname,
                surname,
                imageUrl,
            },
        ]);
    }
}

let singleton: UsersUseCases;

export default function buildUsersUseCase(
    databaseConnection: DatabaseConnection
) {
    if (singleton === undefined) {
        singleton = new UsersUseCases(databaseConnection);
    }

    return singleton;
}
