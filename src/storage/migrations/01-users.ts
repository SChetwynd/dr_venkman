import { Knex } from 'knex';
import debug from 'debug';

const logger = debug('MIGRATIONS');

export async function up(knex: Knex) {
    logger('Users Up');
    await knex.raw(`
        CREATE TABLE users (
            id int NOT NULL AUTO_INCREMENT,
            username varchar(255) NOT NULL,
            firstname varchar(255) NOT NULL,
            surname varchar(255) NOT NULL,
            image_url varchar(255) NOT NULL,
            PRIMARY KEY (id)
        );
    `);
}

export async function down(knex: Knex) {
    logger('Users Down');
    await knex.raw(`DROP TABLE users;`);
}
