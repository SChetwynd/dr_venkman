#!/bin/sh

echo "API: Building"
npm run tsc
echo "API: Built"

echo "API: Copying static routes"
cp -r ./src/server/routes/static ./dist/server/routes

cd ./src/ui
echo "Changed directory: $(pwd)"

echo "UI: Installing dependencies"
npm install
echo "UI: Building"
npm run build
echo "UI: Built"

cd ../../
echo "Changed directory: $(pwd)"

echo "Moving UI"
mv ./src/ui/build/* ./dist/server/routes/static

echo "DB Running migrations"
mkdir ./migrations
cp -r ./dist/storage/migrations/*.js ./migrations
while ! node ./node_modules/knex/bin/cli.js migrate:latest; do
    sleep 1
done
rm -r ./migrations

echo "Build successfull"
