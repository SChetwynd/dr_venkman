import React from 'react';
import { postComment } from './services.js';
import('./reply.css');

export default class Reply extends React.Component {
    constructor(properties) {
        super(properties);
        this.state = {
            newComment: '',
        };
    }

    render() {
        return (
            <div className="reply__container">
                <input
                    type="text"
                    className="reply__input"
                    value={this.state.newComment}
                    onChange={this.updateComment.bind(this)}
                    placeholder="What is your reply?"
                />
                <button
                    className="reply__button"
                    onClick={this.postComment.bind(this)}
                >
                    Reply
                </button>
            </div>
        );
    }

    updateComment(event) {
        this.setState({ newComment: event.target.value });
    }

    async postComment() {
        await postComment(
            this.props.userId,
            this.state.newComment,
            this.props.replyTo
        );
        this.props.toggleSelf();
        this.props.getData();
    }
}
