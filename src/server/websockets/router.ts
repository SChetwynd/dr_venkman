import debug from 'debug';
import WebSocket from 'ws';
import { EventBus } from '../../types';

const logger = debug('WEBSOCKET');

interface Message {
    data: unknown;
    route: Record<string, unknown>;
}

const clients = new Map();

export function router(data: unknown, ws: WebSocket, eventBus: EventBus) {
    if (!isMessage(data)) {
        throw new Error('Invalid message format');
    }

    if (data.route.resource === 'comment') {
        if (data.route.action === 'subscribe') {
            logger('Subscribe to comment');
            if (clients.has(ws) && clients.get(ws).comment) {
                return;
            }
            const listener = eventBus.addEventListener(
                'comment',
                (outData: unknown) => {
                    if (
                        typeof outData !== 'object' ||
                        outData === null ||
                        Array.isArray(outData) ||
                        (outData as Record<string, unknown>).comment_id ===
                            undefined ||
                        (outData as Record<string, unknown>).comment_id !==
                            data.route.id
                    ) {
                        return;
                    }
                    logger(`Emit on comment`);
                    ws.send(JSON.stringify(outData));
                }
            );
            if (!clients.has(ws)) {
                clients.set(ws, {});
            }
            clients.get(ws).comment = listener;
        } else if (data.route.action === 'unsubscribe') {
            logger('Unsubscribe to comment');
            if (!clients.has(ws) || clients.get(ws).comment === undefined) {
                return;
            }
            const client = clients.get(ws);
            delete client.comment;
            if (Object.keys(client).length === 0) {
                clients.delete(ws);
            }
        }
    }
}

function isMessage(data: unknown): data is Message {
    return (
        typeof data === 'object' &&
        !Array.isArray(data) &&
        typeof (data as Record<string, unknown>).data === 'object' &&
        !Array.isArray((data as Record<string, unknown>).data) &&
        typeof (data as Record<string, unknown>).route === 'object' &&
        !Array.isArray((data as Record<string, unknown>).route)
    );
}
