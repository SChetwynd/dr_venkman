import { Knex } from 'knex';
import debug from 'debug';

const logger = debug('MIGRATIONS');

export async function up(knex: Knex) {
    logger('Data Up');
    await knex.raw(`
        INSERT INTO venkman_dev.users
            (id, username, firstname, surname, image_url)
        VALUES
            (1, 'p.venkman', 'Peter', 'Venkman', '/images/venkman.jpg'),
            (2, 'e.spengler', 'Egon', 'Spengler', '/images/spengler.jpg'),
            (3, 'r.stantz', 'Ray', 'Stantz', '/images/stantz.jpg'),
            (4, 'w.zeddemore', 'Winston', 'Zeddemore', '/images/zeddemore.jpg'),
            (5, 'gozer', 'Gozer', 'Gozerian', '/images/gozer.png');
    `);
    await knex.raw(`
        INSERT INTO venkman_dev.posts
            (id, title, body, user)
        VALUES
            (1, 'Test Post', 'Test Post', 1);
    `);
}

export async function down(knex: Knex) {
    logger('Data Down');
    await knex.raw('DROP TABLE comments');
    await knex.raw('DROP TABLE posts');
}
