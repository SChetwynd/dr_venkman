import express from 'express';

import { UseCases } from '../../types';

const router = express.Router();

export default function buildUsers(useCases: UseCases) {
    router.get('/users', async (_request, response) => {
        return response.json(await useCases.users.getUsers());
    });

    router.get('/users/:username', async (request, response) => {
        return response.json(
            await useCases.users.getAUser(request.params.username)
        );
    });

    router.post('/users', async (request, response) => {
        await useCases.users.createAUser(
            request.body.username,
            request.body.firstname,
            request.body.surname,
            request.body.imageUrl
        );
        return response.sendStatus(200);
    });

    return router;
}
