import { DatabaseConnection, EventBus } from '../types';
import buildComments from './comments.js';
import buildUsers from './users.js';

export { CommentsUseCase } from './comments.js';
export { UsersUseCases } from './users.js';

export default function buildUseCases(
    databaseConnection: DatabaseConnection,
    eventBus: EventBus
) {
    return {
        comments: buildComments(databaseConnection, eventBus),
        users: buildUsers(databaseConnection),
    };
}
