import process from 'node:process';
import 'dotenv/config';

export default {
    port: process.env.PORT || 3000,
    dbType: process.env.DB_TYPE,
    dbHost: process.env.DB_HOST,
    dbPort: Number(process.env.DB_PORT),
    dbUser: process.env.DB_USER,
    dbPassword: process.env.DB_PASSWORD,
    dbDatabase: process.env.DB_DATABASE,
};
