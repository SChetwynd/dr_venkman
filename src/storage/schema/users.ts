import debug from 'debug';
import { Knex } from 'knex';

const logger = debug('Storage:Users');

type User = {
    id: number;
    username: string;
    firstname: string;
    surname: string;
    imageUrl: string;
};

type RawUser = {
    username: string;
    firstname: string;
    surname: string;
    imageUrl: string;
};

export class Users {
    private knex: Knex;

    constructor(knexInst: Knex) {
        this.knex = knexInst;
    }

    public getAUser(username: string): Promise<User> {
        logger(`Querying for ${username} user`);
        return this.knex<User>('users')
            .select('id')
            .select('username')
            .select('firstname')
            .select('surname')
            .select('image_url')
            .where({
                username,
            })
            .first();
    }

    public getAllUsers(): Promise<User[]> {
        logger('Getting all users');
        return this.knex<User>('users')
            .select('username')
            .select('firstname')
            .select('surname')
            .select('image_url');
    }

    public createUsers(users: RawUser[]): Promise<void> {
        logger('Creating new user');
        return this.knex('users').insert(
            users.map((user) => ({
                username: user.username,
                firstname: user.firstname,
                surname: user.surname,
                image_url: user.imageUrl,
            }))
        );
    }
}

let singleton: Users;

export function userSchema(knexInst: Knex): Users {
    if (singleton === undefined) {
        logger('Creating new Users singleton');
        singleton = new Users(knexInst);
    }

    return singleton;
}
