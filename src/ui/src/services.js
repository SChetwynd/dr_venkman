const baseURL =
    process.env.REACT_APP_BASE_URL ||
    window.location.protocol + '//' + window.location.host;

export function buildUrl(url) {
    return baseURL + url;
}

export function postComment(userId, comment, replyTo) {
    return fetch(buildUrl('/api/comments'), {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            user: userId,
            post: 1,
            body: comment,
            replyTo,
        }),
    });
}

export async function getComments() {
    const response = await fetch(buildUrl('/api/comments?post=1'));
    return response.json();
}

export async function getUser() {
    const response = await fetch(buildUrl(`/api/users/${randomUser()}`));
    return response.json();
}

export function upvote(commentId) {
    return fetch(buildUrl(`/api/comments/${commentId}/upvote`), {
        method: 'POST',
    });
}

function randomUser() {
    const randomIndex = Math.floor(Math.random() * 5);
    return ['gozer', 'p.venkman', 'e.spengler', 'r.stantz', 'w.zeddemore'][
        randomIndex
    ];
}
