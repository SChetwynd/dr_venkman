import { webSocket } from './websocket.js';
import React from 'react';
import { formatDistanceToNow } from 'date-fns';
import { upvote } from './services.js';

import Reply from './reply.js';
import ProfileIcon from './profile-icon.js';
import('./comment-element.css');

export default class Comment extends React.Component {
    constructor(properties) {
        super(properties);
        this.state = {
            votes: this.props.votes,
            showReply: false,
        };
        this.isWebSocketSetup = false;
    }

    render() {
        if (this.props.commentId !== null && !this.isWebSocketSetup) {
            this.setupWebSocket();
        }
        return (
            <div className="comment__main">
                <span className="inline_block comment__span">
                    <ProfileIcon
                        url={this.props.imageURL}
                        alt={this.props.username}
                    />
                </span>
                <span className="inline_block comment">
                    <div>
                        <span
                            id="firstname"
                            className="comment__name comment__span"
                        >
                            {this.props.firstname}
                        </span>
                        <span
                            id="surname"
                            className="comment__name comment__span"
                        >
                            {this.props.surname}
                        </span>
                        <span
                            id="timestamp"
                            className="comment__time comment__span"
                        >
                            ・{' '}
                            {formatDistanceToNow(
                                new Date(this.props.timestamp)
                            )}{' '}
                            ago
                        </span>
                    </div>
                    <div id="body" className="comment__body">
                        {this.props.body}
                    </div>
                    <div>
                        <span className="comment__votes comment__span">
                            {this.state.votes}
                        </span>
                        <button
                            id="upvote"
                            className="comment__button"
                            onClick={this.upvote.bind(this)}
                        >
                            Upvote
                        </button>
                        <button
                            className="comment__button"
                            onClick={this.toggleReply.bind(this)}
                        >
                            Reply
                        </button>
                    </div>
                    {this.replyInput()}
                    {this.replies()}
                </span>
            </div>
        );
    }

    replyInput() {
        if (this.state.showReply) {
            return (
                <Reply
                    userId={this.props.userId}
                    replyTo={this.props.commentId}
                    getData={this.props.getData}
                    toggleSelf={this.toggleReply.bind(this)}
                />
            );
        }
        return;
    }

    toggleReply() {
        this.setState({ showReply: !this.state.showReply });
    }

    replies() {
        if (this.props.replies.length > 0) {
            return (
                <div className="comment__reply">
                    <div className="comment__reply__vr" />
                    <div className="comment__reply__container">
                        {this.props.replies.map((comment, index) => (
                            <Comment
                                key={index}
                                firstname={comment.firstname}
                                surname={comment.surname}
                                username={comment.username}
                                imageURL={comment.image_url}
                                timestamp={comment.timestamp}
                                body={comment.body}
                                votes={comment.votes}
                                commentId={comment.comment_id}
                                replies={comment.replies}
                                userId={this.props.userId}
                                getData={this.props.getData}
                            />
                        ))}
                    </div>
                </div>
            );
        }
        return;
    }

    setupWebSocket() {
        this.isWebSocketSetup = true;
        webSocket.send(
            JSON.stringify({
                route: {
                    id: Number(this.props.commentId),
                    resource: 'comment',
                    action: 'subscribe',
                },
                data: {},
            })
        );

        webSocket.addEventListener('message', (data) => {
            const parsedData = JSON.parse(data.data);
            if (parsedData.comment_id !== Number(this.props.commentId)) {
                return;
            }
            this.setState({ votes: parsedData.votes });
        });
    }

    async upvote() {
        await upvote(this.props.commentId);
    }
}
