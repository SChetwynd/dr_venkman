import { DatabaseConnection, Comment, EventBus } from '../types';

interface OutComment extends Comment {
    replies: OutComment[];
}

export class CommentsUseCase {
    private databaseConnection: DatabaseConnection;
    private eventBus: EventBus;

    constructor(databaseConnection: DatabaseConnection, eventBus: EventBus) {
        this.databaseConnection = databaseConnection;
        this.eventBus = eventBus;
    }

    async getComments(post: number) {
        const comments = await this.databaseConnection.comments.getComments(
            post
        );

        const commentsObject: Record<string, OutComment> = {};
        for (const comment of comments) {
            commentsObject[comment.comment_id] = {
                ...comment,
                replies: [],
            };
        }

        for (const comment in commentsObject) {
            const currentComment = commentsObject[comment];
            if (currentComment.reply_to !== null) {
                const inReplyTo = commentsObject[currentComment.reply_to];
                inReplyTo.replies.push(currentComment);
            }
        }

        return Object.values(commentsObject).filter(
            (comment) => comment.reply_to === null
        );
    }

    createComment(post: number, user: number, body: string, replyTo: number) {
        return this.databaseConnection.comments.createComments([
            {
                post,
                user,
                body,
                reply_to: replyTo,
            },
        ]);
    }

    async upvote(commentId: number) {
        const result = await this.databaseConnection.comments.upvote(commentId);
        this.eventBus.emit(
            'comment',
            await this.databaseConnection.comments.getComment(commentId)
        );

        return result;
    }
}

let singleton: CommentsUseCase;

export default function buildComments(
    databaseConnection: DatabaseConnection,
    eventBus: EventBus
) {
    if (singleton === undefined) {
        singleton = new CommentsUseCase(databaseConnection, eventBus);
    }
    return singleton;
}
