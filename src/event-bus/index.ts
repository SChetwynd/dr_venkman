export class EventBus {
    private events: Record<
        string,
        Record<symbol, (...arguments_: unknown[]) => unknown>
    >;

    constructor() {
        this.events = {};
    }

    addEventListener(
        event: string,
        callback: (...arguments_: unknown[]) => unknown
    ) {
        if (this.events[event] === undefined) {
            this.events[event] = {};
        }

        this.events[event][Symbol()] = callback;
    }

    removeEventListener(id: symbol) {
        for (const event in this.events) {
            if (this.events[event][id] !== undefined) {
                delete this.events[event][id];
            }
        }
    }

    emit(event: string, data: unknown) {
        if (this.events[event]) {
            for (const sym of Object.getOwnPropertySymbols(
                this.events[event]
            )) {
                (
                    this.events[event][sym] as (
                        ...arguments_: unknown[]
                    ) => unknown
                )(data);
            }
        }
    }
}

let eventBus: EventBus | undefined;

export default function createEventBus() {
    if (!eventBus) {
        eventBus = new EventBus();
    }
    return eventBus;
}
