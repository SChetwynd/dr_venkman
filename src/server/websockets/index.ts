import debug from 'debug';
import { WebSocketServer } from 'ws';
import http from 'node:http';
import streams from 'node:stream';
import { EventBus } from '../../types';
import { router } from './router.js';

const logger = debug('WEBSOCKET');

export default function initWebSocketServer(
    server: http.Server,
    eventBus: EventBus
) {
    const wss = new WebSocketServer({ noServer: true });

    logger('WebSockets set up');

    server.on(
        'upgrade',
        async (
            request: http.IncomingMessage,
            socket: streams.Duplex,
            head: Buffer
        ) => {
            logger('Upgrade request received');
            wss.handleUpgrade(request, socket, head, (ws) => {
                wss.emit('connection', ws, request);
            });
        }
    );

    wss.on('connection', (ws) => {
        logger('Connection received');
        ws.on('message', (data) => {
            const parsedData = JSON.parse(String(data));
            router(parsedData, ws, eventBus);
        });
    });
}
