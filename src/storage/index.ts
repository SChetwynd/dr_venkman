import debug from 'debug';
import knex from 'knex';

import { Knex } from 'knex';

import config from '../config.js';

import {
    Comments,
    commentSchema,
    Posts,
    postSchema,
    Users,
    userSchema,
} from './schema/index.js';

const logger = debug('Storage');

let output: {
    comments: Comments;
    posts: Posts;
    users: Users;
};

switch (config.dbType) {
    case 'mysql2': {
        logger('Creating MySQL Database Connection');
        const knexInst = createMySQLConnection();

        output = {
            comments: commentSchema(knexInst),
            posts: postSchema(knexInst),
            users: userSchema(knexInst),
        };
        break;
    }
    default: {
        throw new Error(`Invalid Database Type "${config.dbType}".`);
    }
}

Object.freeze(output);

export default output;
export { Comments, Comment } from './schema/comments.js';
export { Posts } from './schema/posts.js';
export { Users } from './schema/users.js';

function createMySQLConnection(): Knex {
    return knex({
        client: 'mysql2',
        connection: {
            host: config.dbHost,
            port: config.dbPort,
            user: config.dbUser,
            password: config.dbPassword,
            database: config.dbDatabase,
        },
    });
}
