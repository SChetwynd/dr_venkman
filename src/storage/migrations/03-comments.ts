import { Knex } from 'knex';
import debug from 'debug';

const logger = debug('MIGRATIONS');

export async function up(knex: Knex) {
    logger('Comments Up');
    await knex.raw(`
        CREATE TABLE comments (
            id int NOT NULL AUTO_INCREMENT,
            user int NOT NULL,
            post int NOT NULL,
            timestamp timestamp NOT NULL DEFAULT now(),
            body varchar(255) NOT NULL,
            votes int NOT NULL DEFAULT 0,
            PRIMARY KEY (id),
            FOREIGN KEY (user) REFERENCES users(id),
            FOREIGN KEY (post) REFERENCES posts(id)
        );
    `);
}

export async function down(knex: Knex) {
    logger('Comments Down');
    await knex.raw('DROP TABLE comments');
}
