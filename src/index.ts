import databaseConnection from './storage/index.js';
import buildUseCases from './use-cases/index.js';
import buildServer from './server/index.js';
import eventBus from './event-bus/index.js';

const eventBusInst = eventBus();

const useCases = buildUseCases(databaseConnection, eventBusInst);
buildServer(useCases, eventBusInst);
