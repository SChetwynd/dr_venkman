export { commentSchema, Comments, Comment } from './comments.js';
export { postSchema, Posts } from './posts.js';
export { userSchema, Users } from './users.js';
