#!/bin/sh

while ! node ./node_modules/knex/bin/cli.js migrate:latest; do
    sleep 1
done

npm run start
