import { Comments, Posts, Users } from './storage/index.js';
import { CommentsUseCase, UsersUseCases } from './use-cases/index.js';

export { Comment } from './storage/index.js';
export { EventBus } from './event-bus/index.js';

export declare type DatabaseConnection = {
    comments: Comments;
    posts: Posts;
    users: Users;
};

export declare type UseCases = {
    comments: CommentsUseCase;
    users: UsersUseCases;
};
